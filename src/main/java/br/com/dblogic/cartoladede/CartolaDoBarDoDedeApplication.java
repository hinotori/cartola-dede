package br.com.dblogic.cartoladede;

import br.com.dblogic.cartoladede.robot.CartolaRobot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartolaDoBarDoDedeApplication implements CommandLineRunner {

	@Autowired
	private CartolaRobot cartolaRobot;

	public static void main(String[] args) {
		SpringApplication.run(CartolaDoBarDoDedeApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		cartolaRobot.seekAndDestroy();
	}
}