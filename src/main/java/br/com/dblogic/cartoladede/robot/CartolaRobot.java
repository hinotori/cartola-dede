package br.com.dblogic.cartoladede.robot;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class CartolaRobot {

    private static final Logger logger = LoggerFactory.getLogger(CartolaRobot.class);

    WebDriver driver;

    @PostConstruct
    public void init() {
        logger.info("### Initializing ###");
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
    }

    public void seekAndDestroy() throws InterruptedException {
        logger.info("### Running ###");
        driver.get("https://ge.globo.com/cartola-fc/");
        Thread.sleep(5000);
    }

    @PreDestroy
    public void shutdown() {
        logger.info("### Destroy ###");
        driver.quit();
    }


}